#include "Arduino.h"
#include <ESP8266WiFi.h>

#define ANALOG_PIN A0

// These were measured with the sensor held in air (0%), and inside a glass of water (100%).
// What I have found out is each sensor has its own variations, and each board somehow powers
// the sensor differently. It should be parametrized, and adjusted during runtime.
// Maybe a calibration routine being triggered from the MQTT broker?
unsigned int sensor_dry = 661;
unsigned int sensor_wet = 319;

// A board ID, to be initialized from the mac address of this board.
char board_id[17] = "";

const char* wifi_ssid = "internet-of-shit";
const char* wifi_pass = "";

const unsigned long wifi_retry_interval = 120 * 1000;
unsigned long long last_wifi_retry = 0;

WiFiClient wifi_client;

// America/Sao_Paulo: GMT-3
const short timezone = -3;
// Daylight savings time: the (idiot we got as) president here in Brazil abolished it.
const unsigned short dst_secs = 0;

int current_moisture = 0;

// Turns the led on and off.
void led (bool on) {
  digitalWrite(LED_BUILTIN, on ? LOW : HIGH);
}

bool wifi_connected () {
  return WiFi.status() == WL_CONNECTED;
}

// Reconnects the wifi at board initialization or after a disconnection, waiting some seconds in-between
void wifi_reconnect () {
  bool force_reconnect = last_wifi_retry == 0;
  if (force_reconnect || (!wifi_connected() && ((millis() - last_wifi_retry) > wifi_retry_interval))) {
    last_wifi_retry = millis();
    Serial.printf("Connecting to wifi network %s ", wifi_ssid);
    unsigned short count = 0;
    WiFi.disconnect();
    WiFi.mode(WIFI_STA);
    WiFi.begin(wifi_ssid, wifi_pass);
    while (!wifi_connected() && (count < 30)) {
      count++;
      delay(1000);
      Serial.print(" ... ");
    }
    if (wifi_connected()) {
      const signed long timezone_seconds = timezone * 3600;
      configTime(timezone_seconds, dst_secs, "10.1.2.1", "pool.ntp.br", "pool.ntp.org");
      Serial.printf(" connected with address %s .\n", WiFi.localIP().toString().c_str());
    } else {
      Serial.printf(" fail. Will retry in %d seconds.\n", (int)(wifi_retry_interval / 1000));
    }
  }
}

unsigned int read_humidity_sensor () {
  return analogRead(ANALOG_PIN);
}

unsigned short calculate_moisture(int reading, int dry_reading, int wet_reading) {
  int result = map(reading, dry_reading, wet_reading, 0, 100);
  if (result > 100) {
    result = 100;
  }
  if (result < 0) {
    result = 0;
  }
  return result;
}

// Current date and time in iso 8601 - 2022-02-04T13:24:34+00:00
String current_datetime() {
  time_t now = time(nullptr);
  struct tm* time_info = localtime(&now);
  char result[32] = "";
  sprintf(
    result,
    "%04d-%02d-%02dT%02d:%02d:%02d%s%02d:%02d",
    time_info->tm_year + 1900,
    time_info->tm_mon + 1,
    time_info->tm_mday,
    time_info->tm_hour,
    time_info->tm_min,
    time_info->tm_sec,
    timezone >= 0 ? "+" : "-",
    abs(timezone),
    0 // I know there are some weird timezones with 30-minute offsets, but it's not a problem now ...
  );
  return String(result);
}

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  led(true);
  
  Serial.begin(115200);
  
  WiFi.disconnect();
  WiFi.mode(WIFI_STA);

  // Setting the board ID as its mac address. Do pay attention that the interface must be previously
  // initialized, so that's why the WiFi was disconnected and then set to the WIFI_STA mode.
  byte mac[6];
  WiFi.macAddress(mac);
  sprintf(board_id, "ivy-%02x%02x%02x%02x%02x%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  led(false);
}

void loop() {
  led(true);

  if (last_wifi_retry > millis()) {
    last_wifi_retry = 0;
  }
  if (!wifi_connected()) {
    wifi_reconnect();
  }

  unsigned int reading = read_humidity_sensor();
  current_moisture = calculate_moisture(reading, sensor_dry, sensor_wet);
  Serial.printf("%s Calculating moisture from %06d ( dry: %06d / wet: %06d ) : %d%% .\n", current_datetime().c_str(), reading, sensor_dry, sensor_wet, current_moisture);

  led(false);

  delay(1000);
}
